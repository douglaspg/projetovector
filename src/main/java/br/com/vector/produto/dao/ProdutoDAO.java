package br.com.vector.produto.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.com.vector.produto.model.Produto;

public class ProdutoDAO {

	public int cadastraProduto(Produto produto) throws ClassNotFoundException, SQLException {
		String insertProduto = "INSERT INTO produto(codigo, descricao, valor) VALUES (?,?,?) ";

		int result = 0;
		Class.forName("com.mysql.jdbc.Driver");

		try (Connection con = DriverManager.getConnection("jdbc:mysql://192.168.0.115/vector", "root", "");
				PreparedStatement pst = con.prepareStatement(insertProduto)) {
			pst.setString(1, produto.getCodigo());
			pst.setString(2, produto.getDescricao());
			pst.setDouble(3, produto.getValor());

			System.out.println(pst);
			result = pst.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}
