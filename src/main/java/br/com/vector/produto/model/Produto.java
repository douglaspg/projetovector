package br.com.vector.produto.model;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.NamedEvent;
import javax.servlet.ServletException;

import br.com.vector.produto.controller.ProdutoServlet;

@NamedEvent
@ManagedBean
@RequestScoped
public class Produto {

	private String codigo;
	private String descricao;
	private Double valor;
	private ProdutoServlet pServlet;

	public void runServlet() throws ServletException {
		this.pServlet = new ProdutoServlet();
		getpServlet();
	}

	public void limpaCampos() {
		System.out.println("Limpar campos!");
	}

	public Produto() {
	}

	public void sucesso() {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Produto cadastrado com sucesso!"));
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public ProdutoServlet getpServlet() {
		return pServlet;
	}

	public void setpServlet(ProdutoServlet pServlet) {
		this.pServlet = pServlet;
	}

	@Override
	public String toString() {
		return "Produto: [codigo=" + codigo + ", descricao=" + descricao + ", valor=" + valor + "]";
	}

}
